import 'package:flutter/material.dart';
import 'package:navigator_page/model/data_model.dart';
import 'package:navigator_page/pages/detail.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  static List<String> name = [
    'Test 1 ',
    'Test 2',
    'Test 3',
  ];

  static List url = [
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADgCAMAAADCMfHtAAAAhFBMVEX///8AAAD8/Pz5+flRUVHx8fHr6+v19fXi4uIbGxtfX1/Z2dm9vb1UVFTExMTV1dWrq6uJiYnKysoxMTG2traQkJDg4OCfn582NjYODg6lpaV6enoqKip9fX1qampvb29FRUUgICCYmJiwsLA9PT1kZGSNjY1CQkIWFhYdHR2EhIQzMzOTSGtpAAANdElEQVR4nNVd12LqMAwtmwJllVWgkFBKKf3//7sNUAjoyPGQEu55ZMRRYmscSfbTkzLKzd5oPl6sXr5ev/dvv3hebl9W68H8Y9huaA+ujEavf6g+l4x4bY1H7UrRd+qDdn/9bZYtjXjdbxd9xy6YzFv2wl1R3fTKRd+6DbqLNx/xzniZN4sWwIzOZ4B0Z8Tjh52wvXW4eCc8jydFC0PRmO6l5DtiNn8sBdsTmJ0Eq17RYl3QmSnIl2D5UbRoR3zITs87bAqfrH1N8Y441IqUb5Thk8ngvTD3dfiah3wJBoXM1bqXa+aLaf4CjvOU7xfP3XzlG6oqUIxqPT/5yguvW4xnUYLt0lfGTV4CDt3u6+Uw77Sbt4FRud7u9ncrV1W1zMcpt3+By0U/i6iod6crh1C5NNCXbxLb3crPbmhtxmrdgbXjt9QOO+ZWt/E5cnZEKh3buaFrOGxiiFXHl4xoj2MbEat69r+efQPbUdjwk4GNjFpxVTdz5HcJlmVo4SzNBcahmGaM+jaXospqm0wRV0JDpZGhCOKR6GijLLdgJh5wvOQpX4LuNkNGWbNRNturvuhgfxj+mEUcCo7ViE0jjQVHukXHHGHL0Tg10zAtVZff7GFIGf+6aRDtsK1h5Jllpo9JwIXICGb0TGr1IDCAaYpKrnUDTOYxXMQGn0r6zC0ZNjG8xtB4qsIHb7kS0gZ3NXAtsnZwn3Oiz0AsBGlU1pNpSd25NSoRK2KAQ8X6oju5O7cHP1O9NR4bTch7oVbosCJ6Lhl26heW2JuwInpFGqylF9Ixk+lq+zP7Wk0dKMIaZza2PnfAeL3fMtmudAAYO8QmX4yIHiExQzotRWig5l1g9Gz/HjmOw5nYYJz6pYgfA1SG/Q2uGBEd+XBmTcu8QZg4tk9KcNGG273h3NKziIAMZ2fvBTIivrjcBHMNESXDBiv2OprREQ5LkTGtMmaC9QR/gq9hfYMV/H8ZQ9/jBCyVOvZXwUTcq+3f8SQQctU4VfiLyP4qZawoLCMprAmEGLUyL2Cp5EBqMR6XFYmK70EqXDImkF2mCZ7tVmv5Hf3z2VMgAiM96MRI4HIsi3AY23oxUnRnktBtomCTlm3RoJaSCwiNCZ6q27VgoUPmUxqhf6195aGAa8BTQqxtsgJ+9J83b3kojLVUruoMeiZL838g+SpZxWIs13TmPqFxNTq4DfQPUdqpbZLQfblDvtr0B7RKYm9pIEwSunv20LwaAjG4dIUrrQzstVP4cwbUzXyQh34unV4ypLJ8XHvogbHrCg4unn5hX+Kn1+WgPuVeInqFCuQvxwh6PssquBQTJiBFOvMXhEVIWADQRBfDP0XGWIXehvfkPxIyAJDQQGtWKcVUI0m77wCGBFES0A1D3oZakvDOdwpL5CJHDDEiMf2ZoMd9j8b44o68jUM5PCBhRH+FvAPd6vhmZ7PbbToC8wSF1VRxAfpJ8RUKA0hIijSQqXjwntwUUC73/jfgReefq/cGsgP3vgpgBB6nhzMbgP65IwwA/6ThzqgBJUNu9SRwhwsqR/AEUJS3fg2IlQu6VU8Agnib8X0O/TeiAGUH6WkKFur/YypOAAYjPU3pA/gq7FY9AXRNdP0WhDM6xemaAFnTa6gP3vD/t/3PBxXiGmBQIsCH9ioYIExcGL57jP0a3ECn6SUnCAKn/2+Swmn6ZxCorYgKvFFvAG36NxVpyjCXBv/6cL6oJoiEWiZp9HCOcEHoodxs2+xO1ykyyjFtyILOxTMhBVw2oSEJKpPOhvaoS/VLAn1yYoCoNVToX6y0R7sWsxODlIMIbMKpaYmmGUVtRa33MTB2L8qlmGl57SnRRiMnqWU47L9H2du3SfQunUAX4pGJAUpWaEBjV98Vct1ToJor+ZgqGimXDZhgBKHRnmDOJ3FdKJsv1RRq7h2Wfp4J6NUTOu1APnWogjTiZb1YrDN3vJCM0+gzTdgmGljIhveNjO56ydQBVTVJwjsmnwoOeQTf6lLKLPBxA135SeqcfChPlBrqgmUZL1qts0X6x69iwAjDVheinbbQ8tHnq9Bfb7AbsgPR65dBtYYCCcVviyI8YajmbgJzqNChzXcOCtMldDn0QBJcYUMt3mAI7/tI6zK64DMFjoatDHboIrECfZQfoFpaYRsmdicWaa1GE71zMHOFB30yNVlIZ2Hpgt88kSZw4XrSBLzFlx6JEhkDSlBZNw/Zg20KF6dL6LNcPMX3H3k1DZuBygePEM8zU7dtTTkMKW4vBU5AebVNs2grKqF8UobtdZKfLkhC8pG8hDTGPkN+Q1I0S8lH8oVC7H5W8t4TnS6LHN4hLJg9QnokbC30NQ3bkKdQGUjbtjZ0Cokvf9ZWSDFeKSCvTd3iM33TJRUXn9aqj/S9NjY2jIQHSkCDmKG+581GThppWEqYtvWjJ05AlTQs7VapgwhYtrqbb8cTHeYMOkoZRMWyMRtLYLyLDnMCpUb3KDEjq8TZ8F5jT0lq8L8Q0SeqAfgcosae1fR1rdE6Ee04hL3hCVTqyqhS2aCHLOq2sRt9qGxYTZdEsuTo4JJjcgLqVOjSYRKTRPcjFjQXLAclttFGGiCNl6x2Oo8EaX12Gwy5AowUaALomJ2kBksw9Ga3VlXZvpY6iEetST1juSifb9wWGyKNmAxz1GcgBBcbkt0jQqWnCjzPk39GPxfzidl6E5VCeWB7T1/QGFxqfD5fodK7ScOkM19B42KpOcSmfkULMC6g4+zY+xAakt1uR2WXXmB7z949aB8V0uVsXaKKrQC298+7p/XRMkUueRKlT8hWRH9f0eZDmaJWNqmmULEDXbaL6wIyJyJ8O7sDt0rzJtj04iIFYDQlpinckukIlYM3QRH59UtgmAWGZIlSlQ5joElToTxwrgS0HXv2hsopf2C0FBMEHDqBDDsnoEobPHKf0t8DQiw4qZAvUQqm4U1qCyTAghk3lihV2W4DBKI3Cw1xfqFjsgXeGrYClQrc/gLk+AKJYZ4o1ehtBNbgjicBoVWgTmeLZhXqdaB/eOe0IFUUZjDYwyA1bAUwFSRAA1m+sJZ8TkCN8lVEB5EoHun2kJfI74gccFEOKAlL0yLAJIasRHbvwNusSEOC+kavEOTukBcZoE5ZovT2muvSITwHhdxD9OTAz/ypd54ovbEVx7kc6sQhRQqzSyhe9XZsWKI0Sv+qHPYYz0D1OlCFwLoXX+vMEqU3z+yTvRkHIJ3GcHloZ0jPEIMnStO24uhmBDOXMRiFUSDwJfo9Yb5HJvWj01oN9eHQ4mKLntBL3HsNyxKl6dz2Ud2G9sxA95f17eFL9CoKYRrvb1y2o46PfK6eBlrwBkYd0n8eypy3FddJfxorlJOChRAmMw7vyn1/X7YA45qROTkYoaEipPOMrhi8NXf+li3sviiaoeeV7wCNknnSwcDcOdnG1sz+6a2TgMG2HvoVGeYH00eu7jGraM5X6vhd9h44L5KVmsTVoI5LkRXwVIB85qiC+Rro3Wcm7nDNsmOZFC9hKR4PzpXlwfUmmHHO/h/Wg25W0SDhBcHeGnbubQI+rCacytAsBIy8pEoBV1tZPTeGBnQpB4VH3NwguCGAuUs7D4Kx1w4MkvFAoAQ/wScuYA7B1q4xOyDYK3c+tDghfGNNvJQi279zPSD2RTBmAcOLWYLPZ+SoQGsRjbsLhfdvM16hi4HlTmWyFpH12yQq2Jm7c/NyuSID27VY4XoOW+F5GabEw7F+hG3HstWoDajsYoEWBK6GxTVbwGZwrYkbqg32Ett8cIlX98p4Noq1vs32rdVZiZR6cdkCH1KQLdG214WN0aH6un+Lv9ZToeoEzpfw20GEzQAWd2gCuxuTJ9/DnWNeWuoeXsKhwpYG+OYky2wCSaUtKwt8qaN/LN3gN65U6Zcwgt+JKSRvbjgGbpbzTOX5u7BGRoOIuW5MX2PbGIMpSX7yl0pVlSpKBH6GCnT5mUTM6TWW2SpHmZZ300QtbZX3xU5gOug6Ehmhxu5NkuBdo9s1hYrhMPZSJDUIbVFMQ6Ub9A/G03UFu10NAe0v9mrHCvWMrJ2o95jBni0Vtin5VXKGTUFL4mfAsr0TfzKKv8e6QYMmEN+E1KTRjnibSx4728yQT+OMkXqcJWPpINWg3TPPz5JS5xQfMF4RSUzWUeYO2d9aTrFRdf9hEfZ422ylyhWKIfgktpGxdPAVsrnhQ9IrVDpSLshMupyxGjm75b2d3fPTPpcxU6desDx0rJfLZJ6pW854UfYSf1E2OYr32Lc23Qwxm52x2WW6QT7RjP1rPGP5Oeh32s2bp1+ut7v93cpm3V2xzY1YYKu4s7BfzmaznyVfjmJELoc0ndFk+0P1EOXMDHU834M3CjgXld3xUQMDSafXGhUukSqOVjEk+y8aPIspiKjQs5cbbIpKCjOdMMIB5azgOAhR4fId0eEZ6TC0Huds8ImtR+6CQ2H6BWMk6wRsH/Eo1Po0u2TPErscuHQ/NDeZDEQ23h9n9UHUPiz4HBbL3YOLd0Z7aneq3B3WHw+mW8xo9xcOwd/P++h/O839iPJkNP7MUD/Ll92orU9N6KLWHvanh/WqOlvu336xj3+i1vp90+/09KflP/PYpuj/cCB7AAAAAElFTkSuQmCC',
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADgCAMAAADCMfHtAAAAhFBMVEX///8AAAD8/Pz5+flRUVHx8fHr6+v19fXi4uIbGxtfX1/Z2dm9vb1UVFTExMTV1dWrq6uJiYnKysoxMTG2traQkJDg4OCfn582NjYODg6lpaV6enoqKip9fX1qampvb29FRUUgICCYmJiwsLA9PT1kZGSNjY1CQkIWFhYdHR2EhIQzMzOTSGtpAAANdElEQVR4nNVd12LqMAwtmwJllVWgkFBKKf3//7sNUAjoyPGQEu55ZMRRYmscSfbTkzLKzd5oPl6sXr5ev/dvv3hebl9W68H8Y9huaA+ujEavf6g+l4x4bY1H7UrRd+qDdn/9bZYtjXjdbxd9xy6YzFv2wl1R3fTKRd+6DbqLNx/xzniZN4sWwIzOZ4B0Z8Tjh52wvXW4eCc8jydFC0PRmO6l5DtiNn8sBdsTmJ0Eq17RYl3QmSnIl2D5UbRoR3zITs87bAqfrH1N8Y441IqUb5Thk8ngvTD3dfiah3wJBoXM1bqXa+aLaf4CjvOU7xfP3XzlG6oqUIxqPT/5yguvW4xnUYLt0lfGTV4CDt3u6+Uw77Sbt4FRud7u9ncrV1W1zMcpt3+By0U/i6iod6crh1C5NNCXbxLb3crPbmhtxmrdgbXjt9QOO+ZWt/E5cnZEKh3buaFrOGxiiFXHl4xoj2MbEat69r+efQPbUdjwk4GNjFpxVTdz5HcJlmVo4SzNBcahmGaM+jaXospqm0wRV0JDpZGhCOKR6GijLLdgJh5wvOQpX4LuNkNGWbNRNturvuhgfxj+mEUcCo7ViE0jjQVHukXHHGHL0Tg10zAtVZff7GFIGf+6aRDtsK1h5Jllpo9JwIXICGb0TGr1IDCAaYpKrnUDTOYxXMQGn0r6zC0ZNjG8xtB4qsIHb7kS0gZ3NXAtsnZwn3Oiz0AsBGlU1pNpSd25NSoRK2KAQ8X6oju5O7cHP1O9NR4bTch7oVbosCJ6Lhl26heW2JuwInpFGqylF9Ixk+lq+zP7Wk0dKMIaZza2PnfAeL3fMtmudAAYO8QmX4yIHiExQzotRWig5l1g9Gz/HjmOw5nYYJz6pYgfA1SG/Q2uGBEd+XBmTcu8QZg4tk9KcNGG273h3NKziIAMZ2fvBTIivrjcBHMNESXDBiv2OprREQ5LkTGtMmaC9QR/gq9hfYMV/H8ZQ9/jBCyVOvZXwUTcq+3f8SQQctU4VfiLyP4qZawoLCMprAmEGLUyL2Cp5EBqMR6XFYmK70EqXDImkF2mCZ7tVmv5Hf3z2VMgAiM96MRI4HIsi3AY23oxUnRnktBtomCTlm3RoJaSCwiNCZ6q27VgoUPmUxqhf6195aGAa8BTQqxtsgJ+9J83b3kojLVUruoMeiZL838g+SpZxWIs13TmPqFxNTq4DfQPUdqpbZLQfblDvtr0B7RKYm9pIEwSunv20LwaAjG4dIUrrQzstVP4cwbUzXyQh34unV4ypLJ8XHvogbHrCg4unn5hX+Kn1+WgPuVeInqFCuQvxwh6PssquBQTJiBFOvMXhEVIWADQRBfDP0XGWIXehvfkPxIyAJDQQGtWKcVUI0m77wCGBFES0A1D3oZakvDOdwpL5CJHDDEiMf2ZoMd9j8b44o68jUM5PCBhRH+FvAPd6vhmZ7PbbToC8wSF1VRxAfpJ8RUKA0hIijSQqXjwntwUUC73/jfgReefq/cGsgP3vgpgBB6nhzMbgP65IwwA/6ThzqgBJUNu9SRwhwsqR/AEUJS3fg2IlQu6VU8Agnib8X0O/TeiAGUH6WkKFur/YypOAAYjPU3pA/gq7FY9AXRNdP0WhDM6xemaAFnTa6gP3vD/t/3PBxXiGmBQIsCH9ioYIExcGL57jP0a3ECn6SUnCAKn/2+Swmn6ZxCorYgKvFFvAG36NxVpyjCXBv/6cL6oJoiEWiZp9HCOcEHoodxs2+xO1ykyyjFtyILOxTMhBVw2oSEJKpPOhvaoS/VLAn1yYoCoNVToX6y0R7sWsxODlIMIbMKpaYmmGUVtRa33MTB2L8qlmGl57SnRRiMnqWU47L9H2du3SfQunUAX4pGJAUpWaEBjV98Vct1ToJor+ZgqGimXDZhgBKHRnmDOJ3FdKJsv1RRq7h2Wfp4J6NUTOu1APnWogjTiZb1YrDN3vJCM0+gzTdgmGljIhveNjO56ydQBVTVJwjsmnwoOeQTf6lLKLPBxA135SeqcfChPlBrqgmUZL1qts0X6x69iwAjDVheinbbQ8tHnq9Bfb7AbsgPR65dBtYYCCcVviyI8YajmbgJzqNChzXcOCtMldDn0QBJcYUMt3mAI7/tI6zK64DMFjoatDHboIrECfZQfoFpaYRsmdicWaa1GE71zMHOFB30yNVlIZ2Hpgt88kSZw4XrSBLzFlx6JEhkDSlBZNw/Zg20KF6dL6LNcPMX3H3k1DZuBygePEM8zU7dtTTkMKW4vBU5AebVNs2grKqF8UobtdZKfLkhC8pG8hDTGPkN+Q1I0S8lH8oVC7H5W8t4TnS6LHN4hLJg9QnokbC30NQ3bkKdQGUjbtjZ0Cokvf9ZWSDFeKSCvTd3iM33TJRUXn9aqj/S9NjY2jIQHSkCDmKG+581GThppWEqYtvWjJ05AlTQs7VapgwhYtrqbb8cTHeYMOkoZRMWyMRtLYLyLDnMCpUb3KDEjq8TZ8F5jT0lq8L8Q0SeqAfgcosae1fR1rdE6Ee04hL3hCVTqyqhS2aCHLOq2sRt9qGxYTZdEsuTo4JJjcgLqVOjSYRKTRPcjFjQXLAclttFGGiCNl6x2Oo8EaX12Gwy5AowUaALomJ2kBksw9Ga3VlXZvpY6iEetST1juSifb9wWGyKNmAxz1GcgBBcbkt0jQqWnCjzPk39GPxfzidl6E5VCeWB7T1/QGFxqfD5fodK7ScOkM19B42KpOcSmfkULMC6g4+zY+xAakt1uR2WXXmB7z949aB8V0uVsXaKKrQC298+7p/XRMkUueRKlT8hWRH9f0eZDmaJWNqmmULEDXbaL6wIyJyJ8O7sDt0rzJtj04iIFYDQlpinckukIlYM3QRH59UtgmAWGZIlSlQ5joElToTxwrgS0HXv2hsopf2C0FBMEHDqBDDsnoEobPHKf0t8DQiw4qZAvUQqm4U1qCyTAghk3lihV2W4DBKI3Cw1xfqFjsgXeGrYClQrc/gLk+AKJYZ4o1ehtBNbgjicBoVWgTmeLZhXqdaB/eOe0IFUUZjDYwyA1bAUwFSRAA1m+sJZ8TkCN8lVEB5EoHun2kJfI74gccFEOKAlL0yLAJIasRHbvwNusSEOC+kavEOTukBcZoE5ZovT2muvSITwHhdxD9OTAz/ypd54ovbEVx7kc6sQhRQqzSyhe9XZsWKI0Sv+qHPYYz0D1OlCFwLoXX+vMEqU3z+yTvRkHIJ3GcHloZ0jPEIMnStO24uhmBDOXMRiFUSDwJfo9Yb5HJvWj01oN9eHQ4mKLntBL3HsNyxKl6dz2Ud2G9sxA95f17eFL9CoKYRrvb1y2o46PfK6eBlrwBkYd0n8eypy3FddJfxorlJOChRAmMw7vyn1/X7YA45qROTkYoaEipPOMrhi8NXf+li3sviiaoeeV7wCNknnSwcDcOdnG1sz+6a2TgMG2HvoVGeYH00eu7jGraM5X6vhd9h44L5KVmsTVoI5LkRXwVIB85qiC+Rro3Wcm7nDNsmOZFC9hKR4PzpXlwfUmmHHO/h/Wg25W0SDhBcHeGnbubQI+rCacytAsBIy8pEoBV1tZPTeGBnQpB4VH3NwguCGAuUs7D4Kx1w4MkvFAoAQ/wScuYA7B1q4xOyDYK3c+tDghfGNNvJQi279zPSD2RTBmAcOLWYLPZ+SoQGsRjbsLhfdvM16hi4HlTmWyFpH12yQq2Jm7c/NyuSID27VY4XoOW+F5GabEw7F+hG3HstWoDajsYoEWBK6GxTVbwGZwrYkbqg32Ett8cIlX98p4Noq1vs32rdVZiZR6cdkCH1KQLdG214WN0aH6un+Lv9ZToeoEzpfw20GEzQAWd2gCuxuTJ9/DnWNeWuoeXsKhwpYG+OYky2wCSaUtKwt8qaN/LN3gN65U6Zcwgt+JKSRvbjgGbpbzTOX5u7BGRoOIuW5MX2PbGIMpSX7yl0pVlSpKBH6GCnT5mUTM6TWW2SpHmZZ300QtbZX3xU5gOug6Ehmhxu5NkuBdo9s1hYrhMPZSJDUIbVFMQ6Ub9A/G03UFu10NAe0v9mrHCvWMrJ2o95jBni0Vtin5VXKGTUFL4mfAsr0TfzKKv8e6QYMmEN+E1KTRjnibSx4728yQT+OMkXqcJWPpINWg3TPPz5JS5xQfMF4RSUzWUeYO2d9aTrFRdf9hEfZ422ylyhWKIfgktpGxdPAVsrnhQ9IrVDpSLshMupyxGjm75b2d3fPTPpcxU6desDx0rJfLZJ6pW854UfYSf1E2OYr32Lc23Qwxm52x2WW6QT7RjP1rPGP5Oeh32s2bp1+ut7v93cpm3V2xzY1YYKu4s7BfzmaznyVfjmJELoc0ndFk+0P1EOXMDHU834M3CjgXld3xUQMDSafXGhUukSqOVjEk+y8aPIspiKjQs5cbbIpKCjOdMMIB5azgOAhR4fId0eEZ6TC0Huds8ImtR+6CQ2H6BWMk6wRsH/Eo1Po0u2TPErscuHQ/NDeZDEQ23h9n9UHUPiz4HBbL3YOLd0Z7aneq3B3WHw+mW8xo9xcOwd/P++h/O839iPJkNP7MUD/Ll92orU9N6KLWHvanh/WqOlvu336xj3+i1vp90+/09KflP/PYpuj/cCB7AAAAAElFTkSuQmCC',
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADgCAMAAADCMfHtAAAAhFBMVEX///8AAAD8/Pz5+flRUVHx8fHr6+v19fXi4uIbGxtfX1/Z2dm9vb1UVFTExMTV1dWrq6uJiYnKysoxMTG2traQkJDg4OCfn582NjYODg6lpaV6enoqKip9fX1qampvb29FRUUgICCYmJiwsLA9PT1kZGSNjY1CQkIWFhYdHR2EhIQzMzOTSGtpAAANdElEQVR4nNVd12LqMAwtmwJllVWgkFBKKf3//7sNUAjoyPGQEu55ZMRRYmscSfbTkzLKzd5oPl6sXr5ev/dvv3hebl9W68H8Y9huaA+ujEavf6g+l4x4bY1H7UrRd+qDdn/9bZYtjXjdbxd9xy6YzFv2wl1R3fTKRd+6DbqLNx/xzniZN4sWwIzOZ4B0Z8Tjh52wvXW4eCc8jydFC0PRmO6l5DtiNn8sBdsTmJ0Eq17RYl3QmSnIl2D5UbRoR3zITs87bAqfrH1N8Y441IqUb5Thk8ngvTD3dfiah3wJBoXM1bqXa+aLaf4CjvOU7xfP3XzlG6oqUIxqPT/5yguvW4xnUYLt0lfGTV4CDt3u6+Uw77Sbt4FRud7u9ncrV1W1zMcpt3+By0U/i6iod6crh1C5NNCXbxLb3crPbmhtxmrdgbXjt9QOO+ZWt/E5cnZEKh3buaFrOGxiiFXHl4xoj2MbEat69r+efQPbUdjwk4GNjFpxVTdz5HcJlmVo4SzNBcahmGaM+jaXospqm0wRV0JDpZGhCOKR6GijLLdgJh5wvOQpX4LuNkNGWbNRNturvuhgfxj+mEUcCo7ViE0jjQVHukXHHGHL0Tg10zAtVZff7GFIGf+6aRDtsK1h5Jllpo9JwIXICGb0TGr1IDCAaYpKrnUDTOYxXMQGn0r6zC0ZNjG8xtB4qsIHb7kS0gZ3NXAtsnZwn3Oiz0AsBGlU1pNpSd25NSoRK2KAQ8X6oju5O7cHP1O9NR4bTch7oVbosCJ6Lhl26heW2JuwInpFGqylF9Ixk+lq+zP7Wk0dKMIaZza2PnfAeL3fMtmudAAYO8QmX4yIHiExQzotRWig5l1g9Gz/HjmOw5nYYJz6pYgfA1SG/Q2uGBEd+XBmTcu8QZg4tk9KcNGG273h3NKziIAMZ2fvBTIivrjcBHMNESXDBiv2OprREQ5LkTGtMmaC9QR/gq9hfYMV/H8ZQ9/jBCyVOvZXwUTcq+3f8SQQctU4VfiLyP4qZawoLCMprAmEGLUyL2Cp5EBqMR6XFYmK70EqXDImkF2mCZ7tVmv5Hf3z2VMgAiM96MRI4HIsi3AY23oxUnRnktBtomCTlm3RoJaSCwiNCZ6q27VgoUPmUxqhf6195aGAa8BTQqxtsgJ+9J83b3kojLVUruoMeiZL838g+SpZxWIs13TmPqFxNTq4DfQPUdqpbZLQfblDvtr0B7RKYm9pIEwSunv20LwaAjG4dIUrrQzstVP4cwbUzXyQh34unV4ypLJ8XHvogbHrCg4unn5hX+Kn1+WgPuVeInqFCuQvxwh6PssquBQTJiBFOvMXhEVIWADQRBfDP0XGWIXehvfkPxIyAJDQQGtWKcVUI0m77wCGBFES0A1D3oZakvDOdwpL5CJHDDEiMf2ZoMd9j8b44o68jUM5PCBhRH+FvAPd6vhmZ7PbbToC8wSF1VRxAfpJ8RUKA0hIijSQqXjwntwUUC73/jfgReefq/cGsgP3vgpgBB6nhzMbgP65IwwA/6ThzqgBJUNu9SRwhwsqR/AEUJS3fg2IlQu6VU8Agnib8X0O/TeiAGUH6WkKFur/YypOAAYjPU3pA/gq7FY9AXRNdP0WhDM6xemaAFnTa6gP3vD/t/3PBxXiGmBQIsCH9ioYIExcGL57jP0a3ECn6SUnCAKn/2+Swmn6ZxCorYgKvFFvAG36NxVpyjCXBv/6cL6oJoiEWiZp9HCOcEHoodxs2+xO1ykyyjFtyILOxTMhBVw2oSEJKpPOhvaoS/VLAn1yYoCoNVToX6y0R7sWsxODlIMIbMKpaYmmGUVtRa33MTB2L8qlmGl57SnRRiMnqWU47L9H2du3SfQunUAX4pGJAUpWaEBjV98Vct1ToJor+ZgqGimXDZhgBKHRnmDOJ3FdKJsv1RRq7h2Wfp4J6NUTOu1APnWogjTiZb1YrDN3vJCM0+gzTdgmGljIhveNjO56ydQBVTVJwjsmnwoOeQTf6lLKLPBxA135SeqcfChPlBrqgmUZL1qts0X6x69iwAjDVheinbbQ8tHnq9Bfb7AbsgPR65dBtYYCCcVviyI8YajmbgJzqNChzXcOCtMldDn0QBJcYUMt3mAI7/tI6zK64DMFjoatDHboIrECfZQfoFpaYRsmdicWaa1GE71zMHOFB30yNVlIZ2Hpgt88kSZw4XrSBLzFlx6JEhkDSlBZNw/Zg20KF6dL6LNcPMX3H3k1DZuBygePEM8zU7dtTTkMKW4vBU5AebVNs2grKqF8UobtdZKfLkhC8pG8hDTGPkN+Q1I0S8lH8oVC7H5W8t4TnS6LHN4hLJg9QnokbC30NQ3bkKdQGUjbtjZ0Cokvf9ZWSDFeKSCvTd3iM33TJRUXn9aqj/S9NjY2jIQHSkCDmKG+581GThppWEqYtvWjJ05AlTQs7VapgwhYtrqbb8cTHeYMOkoZRMWyMRtLYLyLDnMCpUb3KDEjq8TZ8F5jT0lq8L8Q0SeqAfgcosae1fR1rdE6Ee04hL3hCVTqyqhS2aCHLOq2sRt9qGxYTZdEsuTo4JJjcgLqVOjSYRKTRPcjFjQXLAclttFGGiCNl6x2Oo8EaX12Gwy5AowUaALomJ2kBksw9Ga3VlXZvpY6iEetST1juSifb9wWGyKNmAxz1GcgBBcbkt0jQqWnCjzPk39GPxfzidl6E5VCeWB7T1/QGFxqfD5fodK7ScOkM19B42KpOcSmfkULMC6g4+zY+xAakt1uR2WXXmB7z949aB8V0uVsXaKKrQC298+7p/XRMkUueRKlT8hWRH9f0eZDmaJWNqmmULEDXbaL6wIyJyJ8O7sDt0rzJtj04iIFYDQlpinckukIlYM3QRH59UtgmAWGZIlSlQ5joElToTxwrgS0HXv2hsopf2C0FBMEHDqBDDsnoEobPHKf0t8DQiw4qZAvUQqm4U1qCyTAghk3lihV2W4DBKI3Cw1xfqFjsgXeGrYClQrc/gLk+AKJYZ4o1ehtBNbgjicBoVWgTmeLZhXqdaB/eOe0IFUUZjDYwyA1bAUwFSRAA1m+sJZ8TkCN8lVEB5EoHun2kJfI74gccFEOKAlL0yLAJIasRHbvwNusSEOC+kavEOTukBcZoE5ZovT2muvSITwHhdxD9OTAz/ypd54ovbEVx7kc6sQhRQqzSyhe9XZsWKI0Sv+qHPYYz0D1OlCFwLoXX+vMEqU3z+yTvRkHIJ3GcHloZ0jPEIMnStO24uhmBDOXMRiFUSDwJfo9Yb5HJvWj01oN9eHQ4mKLntBL3HsNyxKl6dz2Ud2G9sxA95f17eFL9CoKYRrvb1y2o46PfK6eBlrwBkYd0n8eypy3FddJfxorlJOChRAmMw7vyn1/X7YA45qROTkYoaEipPOMrhi8NXf+li3sviiaoeeV7wCNknnSwcDcOdnG1sz+6a2TgMG2HvoVGeYH00eu7jGraM5X6vhd9h44L5KVmsTVoI5LkRXwVIB85qiC+Rro3Wcm7nDNsmOZFC9hKR4PzpXlwfUmmHHO/h/Wg25W0SDhBcHeGnbubQI+rCacytAsBIy8pEoBV1tZPTeGBnQpB4VH3NwguCGAuUs7D4Kx1w4MkvFAoAQ/wScuYA7B1q4xOyDYK3c+tDghfGNNvJQi279zPSD2RTBmAcOLWYLPZ+SoQGsRjbsLhfdvM16hi4HlTmWyFpH12yQq2Jm7c/NyuSID27VY4XoOW+F5GabEw7F+hG3HstWoDajsYoEWBK6GxTVbwGZwrYkbqg32Ett8cIlX98p4Noq1vs32rdVZiZR6cdkCH1KQLdG214WN0aH6un+Lv9ZToeoEzpfw20GEzQAWd2gCuxuTJ9/DnWNeWuoeXsKhwpYG+OYky2wCSaUtKwt8qaN/LN3gN65U6Zcwgt+JKSRvbjgGbpbzTOX5u7BGRoOIuW5MX2PbGIMpSX7yl0pVlSpKBH6GCnT5mUTM6TWW2SpHmZZ300QtbZX3xU5gOug6Ehmhxu5NkuBdo9s1hYrhMPZSJDUIbVFMQ6Ub9A/G03UFu10NAe0v9mrHCvWMrJ2o95jBni0Vtin5VXKGTUFL4mfAsr0TfzKKv8e6QYMmEN+E1KTRjnibSx4728yQT+OMkXqcJWPpINWg3TPPz5JS5xQfMF4RSUzWUeYO2d9aTrFRdf9hEfZ422ylyhWKIfgktpGxdPAVsrnhQ9IrVDpSLshMupyxGjm75b2d3fPTPpcxU6desDx0rJfLZJ6pW854UfYSf1E2OYr32Lc23Qwxm52x2WW6QT7RjP1rPGP5Oeh32s2bp1+ut7v93cpm3V2xzY1YYKu4s7BfzmaznyVfjmJELoc0ndFk+0P1EOXMDHU834M3CjgXld3xUQMDSafXGhUukSqOVjEk+y8aPIspiKjQs5cbbIpKCjOdMMIB5azgOAhR4fId0eEZ6TC0Huds8ImtR+6CQ2H6BWMk6wRsH/Eo1Po0u2TPErscuHQ/NDeZDEQ23h9n9UHUPiz4HBbL3YOLd0Z7aneq3B3WHw+mW8xo9xcOwd/P++h/O839iPJkNP7MUD/Ll92orU9N6KLWHvanh/WqOlvu336xj3+i1vp90+/09KflP/PYpuj/cCB7AAAAAElFTkSuQmCC'
  ];

  final List<DataModel> Fruitdata = List.generate(
      name.length, (index) => DataModel('${name[index]}', '${url[index]}'));

// Drawer
  late Image personAccountImage;
  late Image accountHeaderImage;

  @override
  void initState() {
    super.initState();

    personAccountImage = Image.asset("assets/images/Dog.png");
    accountHeaderImage = Image.asset("assets/images/Dog.png");
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    precacheImage(personAccountImage.image, context);
    precacheImage(accountHeaderImage.image, context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
        itemCount: Fruitdata.length,
        itemBuilder: (context, index) {
          return Container(
            height: 100,
            child: Card(
              child: Align(
                alignment: Alignment.center,
                child: ListTile(
                  title: Text(
                    Fruitdata[index].name,
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  ),
                  leading: SizedBox(
                    width: 70,
                    height: 70,
                    child: Image.network(Fruitdata[index].ImageUrl),
                  ),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => Detail(
                              dataModel: Fruitdata[index],
                            )));
                  },
                ),
              ),
            ),
          );
        },
      ),
      drawer: Drawer(
        child: ListView(padding: const EdgeInsets.all(0.0), children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: const Text('Kampee kantangkun'),
            accountEmail: const Text('6250110001@psu.ac.th'),
            currentAccountPicture: CircleAvatar(
              backgroundImage: personAccountImage.image,
            ),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: accountHeaderImage.image, fit: BoxFit.cover)),
          ),
          ListTile(
              title: const Text('About'),
              leading: const Icon(Icons.info),
              onTap: () {
                Navigator.of(context).pop();
              }),
          const Divider(),
          ListTile(
              title: const Text('Close'),
              leading: const Icon(Icons.close),
              onTap: () {
                Navigator.of(context).pop();
              }),
        ]),
      ),
    );
  }
}
